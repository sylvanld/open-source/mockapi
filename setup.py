import setuptools

setuptools.setup(
    name="mockapi",
    description="Mock a REST API",
    version="0.0.1",
    packages=setuptools.find_packages(),
    entry_points={
        "console_scripts": ["mockapi=mockapi.__main__:cli"]
    },
    install_requires=["click", "fastapi", "uvicorn"]
)
