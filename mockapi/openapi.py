import abc
import json
from typing import Optional

import yaml


class OpenAPIStorage(abc.ABC):
    @abc.abstractmethod
    def save(self, openapi_spec: dict):
        raise NotImplementedError


class OpenAPIDummyStorage(OpenAPIStorage):
    def save(self, openapi_spec: dict):
        return


class OpenAPIJsonStorage(OpenAPIStorage):
    def __init__(self, filename: str):
        self.filename = filename

    def save(self, openapi_spec: dict):
        with open(self.filename, "w", encoding="utf-8") as json_file:
            json.dump(openapi_spec, json_file, indent=4)


class OpenAPIYamlStorage(OpenAPIStorage):
    def __init__(self, filename: str):
        self.filename = filename

    def save(self, openapi_spec: dict):
        with open(self.filename, "w", encoding="utf-8") as yaml_file:
            yaml.safe_dump(openapi_spec, yaml_file)


class OpenAPIStorageFactory:
    @staticmethod
    def from_filename(filename: Optional[str]) -> OpenAPIStorage:
        if filename is None:
            return OpenAPIDummyStorage()
        elif filename.endswith(".json"):
            return OpenAPIJsonStorage(filename)
        else:  # if filename.endswith('.yaml') or filename.endswith('.yml'):
            return OpenAPIYamlStorage(filename)
