import uuid

from pydantic import BaseModel, Field, create_model


def generate_uid() -> str:
    """Generate random hexadecimal uuid"""
    return uuid.uuid4().hex


def schema_from_dict(name: str, data: dict):
    """Given a dictionary, generate corresponding API Read/Write models."""
    fields = {}
    for field_name, value in data.items():
        fields[field_name] = (type(value), ...)

    write_schema = create_model(name.title() + "Write", **fields)
    read_schema = create_model(
        name.title() + "Read", uid=Field("091b7954352d4282ae5458b2a8504daa"), **fields
    )

    return read_schema, write_schema
