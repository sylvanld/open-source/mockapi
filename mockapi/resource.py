from dataclasses import dataclass, field
from typing import Dict, List, Type

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

from mockapi.helpers import generate_uid


@dataclass
class ResourceTable:
    name: str
    read_schema: Type[BaseModel]
    write_schema: Type[BaseModel]
    entries: Dict[str, BaseModel] = field(default_factory=dict)

    def add_item(self, item: BaseModel, uid: str = None):
        item_uid = uid if uid else generate_uid()
        self.entries[item_uid] = self.read_schema.parse_obj(
            {"uid": item_uid, **item.dict()}
        )
        return self.entries[item_uid]

    def search_item(self):
        return list(self.entries.values())

    def get_item(self, uid: str):
        try:
            return self.entries[uid]
        except KeyError:
            raise HTTPException(404, detail=f"No {self.name} found with UID {uid}")

    def delete_item(self, uid: str):
        try:
            del self.entries[uid]
        except KeyError:
            raise HTTPException(404, detail=f"No {self.name} found with UID {uid}")


@dataclass
class ResourceAPI:
    table: ResourceTable

    def register_endpoints(self, api: FastAPI):
        async def create_item(dto: self.table.write_schema):
            return self.table.add_item(dto)

        create_item.__name__ = f"create_{self.table.name}"

        async def update_item(uid: str, dto: self.table.write_schema):
            return self.table.add_item(dto, uid=uid)

        update_item.__name__ = f"update_{self.table.name}"

        async def delete_item(uid: str):
            self.table.delete_item(uid)

        delete_item.__name__ = f"delete_{self.table.name}_by_uid"

        async def search_item():
            return self.table.search_item()

        search_item.__name__ = f"search_{self.table.name}"

        async def get_item(uid: str):
            return self.table.get_item(uid)

        get_item.__name__ = f"get_{self.table.name}_by_uid"

        api.post(
            "/%(tablename)s" % {"tablename": self.table.name},
            response_model=self.table.read_schema,
            status_code=201,
        )(create_item)
        api.put(
            "/%(tablename)s/{uid}" % {"tablename": self.table.name},
            response_model=self.table.read_schema,
        )(update_item)
        api.delete(
            "/%(tablename)s/{uid}" % {"tablename": self.table.name}, status_code=204
        )(delete_item)
        api.get(
            "/%(tablename)s" % {"tablename": self.table.name},
            response_model=List[self.table.read_schema],
        )(search_item)
        api.get(
            "/%(tablename)s/{uid}" % {"tablename": self.table.name},
            response_model=self.table.read_schema,
        )(get_item)
