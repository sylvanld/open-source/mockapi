import json
import json.decoder

from fastapi import FastAPI, HTTPException, Request
from fastapi.encoders import jsonable_encoder
from fastapi.openapi.utils import get_openapi
from fastapi.responses import JSONResponse, Response

from mockapi.helpers import schema_from_dict
from mockapi.openapi import OpenAPIStorageFactory
from mockapi.resource import ResourceAPI, ResourceTable


def create_api(record: str):
    api = FastAPI()
    openapi_storage = OpenAPIStorageFactory.from_filename(record)

    @api.exception_handler(404)
    async def custom_http_exception_handler(request: Request, exc):
        """Handles resource endpoints creation on POST/PUT request when endpoints does not exists"""
        if isinstance(exc, HTTPException) or "favicon.ico" in request.url.path:
            return JSONResponse(
                status_code=exc.status_code,
                content=jsonable_encoder({"detail": exc.detail}),
            )

        try:
            request_json = await request.json()
        except json.decoder.JSONDecodeError:
            request_json = None

        if request.method in ("POST", "PUT") and request_json:
            # generate models for new resource based on endpoint called and json body
            tablename, *_ = request.url.path.strip("/").split("/")
            read_schema, write_schema = schema_from_dict(tablename, request_json)

            # create endpoint for new resource
            resource_table = ResourceTable(tablename, read_schema, write_schema)
            resource_api = ResourceAPI(resource_table)
            resource_api.register_endpoints(api)

            # add item to newly created table
            item = write_schema.parse_obj(request_json)
            model = resource_table.add_item(item)

            # record openapi spec if asked by CLI option
            openapi_storage.save(api.openapi())

            # make normal response
            return JSONResponse(
                status_code=201 if request.method == "POST" else 200,
                content=jsonable_encoder(model),
            )
        return Response(
            "Method: %s, Path: %s, Json: %s"
            % (request.method, request.url.path, request_json)
        )

    return api
