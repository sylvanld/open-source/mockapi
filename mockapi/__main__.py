import click
import uvicorn

from mockapi.api import create_api


@click.command
@click.option(
    "--host",
    type=str,
    default="localhost",
    help="Bind host address for mock api server",
)
@click.option("--port", type=int, default=8090, help="Bind port for mock api server")
@click.option(
    "--record",
    type=str,
    default=None,
    help="Name of the file where API definition will be recorded if specified",
)
def cli(host: str, port: int, record: str):
    api = create_api(record)
    uvicorn.run(api, host=host, port=port)
